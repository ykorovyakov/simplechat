package ru.korovyakov.edu.config;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.commands.AbstractCommand;
import ru.korovyakov.edu.commands.BroadcastMessageCommand;
import ru.korovyakov.edu.commands.CreateChatCommand;
import ru.korovyakov.edu.commands.DropUserCommand;
import ru.korovyakov.edu.commands.ExitCommand;
import ru.korovyakov.edu.commands.HelpCommand;
import ru.korovyakov.edu.commands.JoinCommand;
import ru.korovyakov.edu.commands.ListChatMembersCommand;
import ru.korovyakov.edu.commands.ListChatsCommand;
import ru.korovyakov.edu.commands.LoginCommand;
import ru.korovyakov.edu.commands.NoRightsCommand;
import ru.korovyakov.edu.commands.SendPrivateCommand;
import ru.korovyakov.edu.commands.SendToChatCommand;

import javax.sql.DataSource;
import java.io.IOException;
import java.net.Socket;
import java.util.EnumSet;
import java.util.Map;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.korovyakov.edu")
@PropertySource("classpath:db-conf.properties")
@EnableJpaRepositories(basePackages = "ru.korovyakov.edu.api.repository")
public class SimpleChatConfig {
    private static final EnumSet<UserRole> all = EnumSet.allOf(UserRole.class);
    private static final EnumSet<UserRole> admin = EnumSet.of(UserRole.AdminRole);

    @Bean(name = "loginCommand")
    public LoginCommand loginCommand() {
        return new LoginCommand(all);
    }

    @Bean(name = "defaultCommand")
    public BroadcastMessageCommand defaultCommand() {
        return new BroadcastMessageCommand(all);
    }

    @Bean(name = "noRightsCommand")
    public AbstractCommand noRightsCommand() {
        return new NoRightsCommand(all);
    }

    @Bean
    public AbstractCommand helpCommand() {
        return new HelpCommand(all);
    }

    @Bean
    public AbstractCommand sendPrivateCommand() {
        return new SendPrivateCommand(all);
    }

    @Bean
    public AbstractCommand exitCommand() {
        return new ExitCommand(all);
    }

    @Bean
    public AbstractCommand createChatCommand() {
        return new CreateChatCommand(all);
    }

    @Bean
    public AbstractCommand dropUserCommand() {
        return new DropUserCommand(admin);
    }

    @Bean
    public AbstractCommand joinCommand() {
        return new JoinCommand(all);
    }

    @Bean
    public AbstractCommand listChatsCommand() {
        return new ListChatsCommand(all);
    }

    @Bean
    public AbstractCommand listMembersCommand() {
        return new ListChatMembersCommand(admin);
    }

    @Bean
    public AbstractCommand sendToChatCommand() {
        return new SendToChatCommand(all);
    }

    @Bean
    public Socket socket() throws IOException {
        return new Socket("127.0.0.1", 4888);
    }

    @Bean(name = "commandList")
    public Map<String, AbstractCommand> commandList() {
        return
            ImmutableMap.<String, AbstractCommand>builder()
                .put(helpCommand().name(), helpCommand())
                .put(sendPrivateCommand().name(), sendPrivateCommand())
                .put(exitCommand().name(), exitCommand())
                .put(createChatCommand().name(), createChatCommand())
                .put(dropUserCommand().name(), dropUserCommand())
                .put(joinCommand().name(), joinCommand())
                .put(listChatsCommand().name(), listChatsCommand())
                .put(listMembersCommand().name(), listMembersCommand())
                .put(sendToChatCommand().name(), sendToChatCommand())
            .build();
    }

    @Bean
    public DataSource dataSource(
        @Value("${datasource.driver}") final String dataSourceDriver,
        @Value("${datasource.url}") final String dataSourceUrl,
        @Value("${datasource.user}") final String dataSourceUser,
        @Value("${datasource.password}") final String dataSourcePassword
    ) {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dataSourceDriver);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setUsername(dataSourceUser);
        dataSource.setPassword(dataSourcePassword);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
        final DataSource dataSource,
        @Value("${hibernate.show_sql}") final boolean showSql,
        @Value("${hibernate.hbm2ddl.auto}") final String tableStrategy,
        @Value("${hibernate.dialect}") final String dialect
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.korovyakov.edu.entity");
        final Properties properties = new Properties();
        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.hbm2ddl.auto", tableStrategy);
        properties.put("hibernate.dialect", dialect);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
        final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
