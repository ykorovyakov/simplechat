package ru.korovyakov.edu.runnable;

import org.apache.kafka.common.errors.WakeupException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.korovyakov.edu.api.service.MessageService;
import ru.korovyakov.edu.api.transport.SimpleChatConsumer;
import ru.korovyakov.edu.entity.ChatMessage;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
public class ChatConsumerRunner implements Runnable {
    @Autowired
    @Qualifier("socket_consumer")
    private SimpleChatConsumer consumer;
    private final AtomicBoolean closed = new AtomicBoolean(false);

    @Autowired
    private MessageService messageService;

    @Override
    public void run() {
        closed.set(false);

        while (!closed.get()) {
            try {
                tick();
            } catch (WakeupException | IOException ignored) {
            }
        }
    }

    private void tick() throws IOException {
        final ChatMessage msg = consumer.getMessage();
        if(Objects.nonNull(msg)) {
            messageService.saveMessage(msg);
        }
    }
    public void shutdown() {
        closed.set(true);
        consumer.wakeup();
    }
}
