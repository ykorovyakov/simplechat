package ru.korovyakov.edu.entity;

import ru.korovyakov.edu.entity.User;
import com.google.common.collect.ImmutableList;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "chats")
@NoArgsConstructor
public class Chat {
    @Id
    @SequenceGenerator(name = "USING_SEQ_CHATS", sequenceName = "using_seq_chats", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "USING_SEQ_CHATS", strategy = GenerationType.SEQUENCE)
    @Getter
    private int id;

    @Getter
    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "chats")
    private Set<User> users = new HashSet<>();

    public Chat(final String name) {
        this.name = name;
    }

    public boolean hasUser(final User user) {
        return users.contains(user);
    }

    public boolean subscribe(final User user) {
        if(hasUser(user)) {
           return false;
        }
        users.add(user);
        return true;
    }

    public boolean unsubscribe(final User user) {
        if(!hasUser(user)) {
            return false;
        }

        return users.remove(user);
    }

    public List<User> getSubscriberList() {
        return ImmutableList.copyOf(users);
    }
}
