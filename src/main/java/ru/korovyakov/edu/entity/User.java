package ru.korovyakov.edu.entity;

import com.google.common.collect.ImmutableList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.chat.UserRoleConverter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "users")
@NoArgsConstructor
public class User {
    public static User empty() {
        return new User();
    }

    public static User admin() {
        return new User(UserRole.AdminRole.command(), UserRole.AdminRole);
    }

    @Getter
    @Id
    @SequenceGenerator(name = "USING_SEQ", sequenceName = "using_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "USING_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Getter
    @Column(name = "name")
    private String name;

    @Getter
    @Column
    @Convert(converter = UserRoleConverter.class)
    private UserRole role;

    @ManyToMany(cascade = CascadeType.ALL, fetch= FetchType.EAGER)
    @JoinTable(name = "user_to_chat",
        joinColumns = {@JoinColumn(name = "user_id")},
        inverseJoinColumns = {@JoinColumn(name = "chat_id")})
    private Set<Chat> chats = new HashSet<>();

    public User(final String name, final UserRole role) {
        this.name = name;
        this.role = role;
    }

    public void addChat(final Chat chat) {
        chats.add(chat);
    }

    public void removeChat(final Chat chat) {
        chats.remove(chat);
    }

    public List<Chat> getChatList() {
        return ImmutableList.copyOf(chats);
    }

    public List<String> getChatNameList() {
        return getChatList().stream().map(Chat::getName).collect(Collectors.toList());
    }
}

