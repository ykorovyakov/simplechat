package ru.korovyakov.edu.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "messages")
@Builder
@Getter
public class ChatMessage {
    private static final String EMPTY_TOPIC_NAME = "";

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "chat_name")
    @Builder.Default
    private String topic = EMPTY_TOPIC_NAME;
    @Column(name = "sender")
    private String from;
    @Column(name = "message")
    private String message;

    @Column(name = "is_private")
    private boolean isPrivate;
    @Column(name = "is_chat")
    private boolean isChat;

    public static ChatMessage privateChatMessage(final String from, final String message) {
        return
            ChatMessage.builder()
                .from(from)
                .message(message)
                .isPrivate(true)
                .isChat(false)
            .build();
    }

    public static ChatMessage broadCastChatMessage(final String from, final String message) {
        return
            ChatMessage.builder()
                .from(from)
                .message(message)
                .isPrivate(false)
                .isChat(false)
                .build();
    }

    public static ChatMessage chatMessage(final String topic, final String from, final String message) {
        return
            ChatMessage.builder()
                .topic(topic)
                .from(from)
                .message(message)
                .isPrivate(false)
                .isChat(true)
                .build();
    }

    public Boolean isPrivate() {
        return isPrivate && !isChat;
    }

    public Boolean isBroadcast() {
        return !isPrivate && !isChat;
    }

    public Boolean isChat() {
        return !isPrivate && isChat;
    }
}
