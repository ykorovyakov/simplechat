package ru.korovyakov.edu.chat;

import org.apache.commons.lang3.StringUtils;

public enum UserRole {
    AdminRole ("admin"),
    CommonUserRole("user");

    private String command;

    UserRole(final String command) {
        this.command = command;
    }

    public String command() {
        return command;
    }

    public static UserRole fromCommand(final String command) {
        if(StringUtils.equalsIgnoreCase(AdminRole.command(), command)) {
            return AdminRole;
        }

        return CommonUserRole;
    }
}
