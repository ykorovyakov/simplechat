package ru.korovyakov.edu.chat;

import javax.persistence.AttributeConverter;

public class UserRoleConverter implements AttributeConverter<UserRole, String> {

    @Override
    public String convertToDatabaseColumn(final UserRole role) {
        return role.command();
    }

    @Override
    public UserRole convertToEntityAttribute(final String roleText) {
        return UserRole.fromCommand(roleText);
    }
}