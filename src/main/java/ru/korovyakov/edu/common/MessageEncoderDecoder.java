package ru.korovyakov.edu.common;

import lombok.var;
import ru.korovyakov.edu.entity.ChatMessage;

public class MessageEncoderDecoder {
    private static final int TOPIC_IDX = 0;
    private static final int FROM_IDX = 1;
    private static final int MESSAGE_IDX = 2;
    private static final int IS_PRIVATE_IDX = 3;
    private static final int IS_CHAT_IDX = 4;

    public static String toPublicString(final String text, final String userName) {
        return
            String.format("%s|%s|%s|%s|%s",
                "", userName, text, Boolean.FALSE.toString(), Boolean.FALSE.toString());
    }

    public static String toPrivateString(final String text, final String userName) {
        return
            String.format("%s|%s|%s|%s|%s",
                "", userName, text, Boolean.FALSE.toString(), Boolean.FALSE.toString());
    }


    public static ChatMessage toMessage(final String data) {
        final var raw = data.split("\\|");
        assert raw.length > 5;

        return
            ChatMessage.builder()
                .topic(raw[TOPIC_IDX])
                .from(raw[FROM_IDX])
                .message(raw[MESSAGE_IDX])
                .isPrivate(Boolean.getBoolean(raw[IS_PRIVATE_IDX]))
                .isChat(Boolean.getBoolean(raw[IS_PRIVATE_IDX]))
                .build();
    }
}
