package ru.korovyakov.edu.common;

import org.apache.commons.lang3.StringUtils;
import ru.korovyakov.edu.bootstrap.SimpleChatApp;

public class CommonRoutines {
    public static String makePrivateTopicName(final String userLogin) {
        return String.format("private_for_%s", userLogin);
    }

    public static boolean isPrivateMessage(final String topic, final String userName) {
        final String privateTopicName = CommonRoutines.makePrivateTopicName(userName);
        return StringUtils.equals(privateTopicName, topic);
    }

    public static boolean isBroadCastmessage(final String topic) {
        return StringUtils.equals(topic, SimpleChatApp.PUBLIC_TOPIC_NAME);
    }
}
