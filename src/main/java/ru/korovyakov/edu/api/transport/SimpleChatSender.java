package ru.korovyakov.edu.api.transport;

import ru.korovyakov.edu.entity.User;

import java.io.IOException;

public interface SimpleChatSender {
    void start(User user) throws IOException;

    void sendPublicMessage(String topic, String text) throws IOException;
    void sendPrivateMessage(String userName, String text) throws IOException;
    void sendChatMessage(String chat, String text);
}
