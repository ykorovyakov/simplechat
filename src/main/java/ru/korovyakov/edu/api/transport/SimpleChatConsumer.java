package ru.korovyakov.edu.api.transport;

import ru.korovyakov.edu.entity.Chat;
import ru.korovyakov.edu.entity.ChatMessage;
import ru.korovyakov.edu.entity.User;

import java.io.IOException;
import java.util.List;

public interface SimpleChatConsumer {
    void start(User user, List<String> topicList) throws IOException;
    void stop();
    void wakeup();

    void addSubscription(String topic);
    void removeSubscription(Chat topic, User user);

    ChatMessage getMessage() throws IOException;
}
