package ru.korovyakov.edu.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.korovyakov.edu.entity.Chat;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Integer> {
    Chat getChatByName(String name);
}
