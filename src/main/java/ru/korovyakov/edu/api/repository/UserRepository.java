package ru.korovyakov.edu.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.korovyakov.edu.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User getUserByName(String name);
}
