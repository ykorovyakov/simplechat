package ru.korovyakov.edu.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.korovyakov.edu.entity.ChatMessage;

@Repository
public interface MessageRepository  extends JpaRepository<ChatMessage, Long> {
}
