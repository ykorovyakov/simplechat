package ru.korovyakov.edu.api;

public interface DataProvider {
    String getNextToken();
    void showMessage(String message);
 }
