package ru.korovyakov.edu.api.service;

import ru.korovyakov.edu.entity.ChatMessage;

public interface MessageService {
    void saveMessage(ChatMessage message);
}
