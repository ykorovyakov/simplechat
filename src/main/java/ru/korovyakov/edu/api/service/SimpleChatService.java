package ru.korovyakov.edu.api.service;

import ru.korovyakov.edu.entity.Chat;
import ru.korovyakov.edu.entity.User;
import ru.korovyakov.edu.exceptions.AlreadyJoinedException;
import ru.korovyakov.edu.exceptions.UnknownChatException;
import ru.korovyakov.edu.exceptions.UnknownUserException;

public interface SimpleChatService {
    void createChat(String name, User user);

    void removeUserFromChat(String userName, String chatName) throws UnknownUserException, UnknownChatException;

    void joinChat(String chatName, User user) throws UnknownChatException, AlreadyJoinedException;

    void createUser(User user);

    Chat findChat(String chatName) throws UnknownChatException;

    User findUser(String userName);
}
