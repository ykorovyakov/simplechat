package ru.korovyakov.edu.service;

import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.korovyakov.edu.api.repository.ChatRepository;
import ru.korovyakov.edu.api.repository.UserRepository;
import ru.korovyakov.edu.api.service.SimpleChatService;
import ru.korovyakov.edu.api.transport.SimpleChatConsumer;
import ru.korovyakov.edu.entity.Chat;
import ru.korovyakov.edu.entity.User;
import ru.korovyakov.edu.exceptions.AlreadyJoinedException;
import ru.korovyakov.edu.exceptions.UnknownChatException;
import ru.korovyakov.edu.exceptions.UnknownUserException;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

@Service
public class SimpleChatServiceImpl implements SimpleChatService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    @Qualifier("socket_consumer")
    private SimpleChatConsumer consumer;

    @Override
    @Transactional
    public void createChat(final String name, final User user) {
        final var chat = new Chat(name);

        boolean res = chat.subscribe(user);
        assert res;
        user.addChat(chat);

        consumer.addSubscription(name);

        userRepository.save(user);
    }

    @Override
    @Transactional
    public void removeUserFromChat(final String userName, final String chatName) throws UnknownUserException, UnknownChatException {
        final var chat = findChat(chatName);
        final var user = findUser(userName);

        consumer.removeSubscription(chat, user);
        user.removeChat(chat);
        chat.unsubscribe(user);

        userRepository.save(user);
    }

    @Override
    @Transactional
    public void joinChat(final String chatName, final User user) throws UnknownChatException, AlreadyJoinedException {
        final var chat = findChat(chatName);
        if(!chat.subscribe(user)) {
            throw new AlreadyJoinedException();
        }
        user.addChat(chat);
        consumer.addSubscription(chatName);

        chatRepository.save(chat);
    }

    @Override
    @Transactional
    public void createUser(final User user) {
        userRepository.save(user);    }

    @Override
    @Transactional
    public Chat findChat(final String chatName) throws UnknownChatException {
        try {
            return chatRepository.getChatByName(chatName);
        }
        catch (NoResultException ex) {
            throw new UnknownChatException();
        }
    }

    @Override
    @Transactional
    public User findUser(final String userName) {
        return userRepository.getUserByName(userName);
    }
}
