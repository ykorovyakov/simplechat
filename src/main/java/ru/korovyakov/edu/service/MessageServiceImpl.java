package ru.korovyakov.edu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.korovyakov.edu.api.repository.MessageRepository;
import ru.korovyakov.edu.api.service.MessageService;
import ru.korovyakov.edu.entity.ChatMessage;

import javax.transaction.Transactional;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageRepository repository;

    @Override
    @Transactional
    public void saveMessage(final ChatMessage message) {
        repository.save(message);
    }
}
