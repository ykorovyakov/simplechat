package ru.korovyakov.edu.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.commands.AbstractCommand;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class PermissionHelper {
    @Autowired
    @Qualifier("commandList")
    private Map<String, AbstractCommand> commandList;

    public List<AbstractCommand> commandsByRole(final UserRole role) {
        return
            commandList.values().stream()
                .filter(abstractCommand -> abstractCommand.getPermissions().contains(role))
                .collect(Collectors.toList());
    }
}
