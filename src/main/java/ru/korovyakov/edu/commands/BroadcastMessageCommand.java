package ru.korovyakov.edu.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.korovyakov.edu.api.transport.SimpleChatSender;
import ru.korovyakov.edu.bootstrap.SimpleChatApp;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;

import java.io.IOException;
import java.util.EnumSet;

public class BroadcastMessageCommand extends AbstractCommand {
    @Autowired
    @Qualifier("socket_sender")
    private SimpleChatSender sender;

    private String message;

    public BroadcastMessageCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User user) {
        try {
           sender.sendPublicMessage(SimpleChatApp.PUBLIC_TOPIC_NAME, message);
        }
        catch (IOException ex) {

        }
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public String description() {
        return null;
    }

    void setMessage(final String message) {
        this.message = message;
    }
}
