package ru.korovyakov.edu.commands;

import lombok.var;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;
import ru.korovyakov.edu.exceptions.UnknownChatException;
import ru.korovyakov.edu.exceptions.UnknownUserException;

import java.util.EnumSet;

public class DropUserCommand extends InteractiveChatCommand {
    public DropUserCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User user) {
        getDataProvider().showMessage("Enter chat name:");
        final var chatName = getDataProvider().getNextToken();

        getDataProvider().showMessage("Enter user name drop to:");
        final var userName = getDataProvider().getNextToken();

        try {
            getService().removeUserFromChat(userName, chatName);
        } catch (UnknownUserException e) {
            getDataProvider().showMessage("Unknown user");
        } catch (UnknownChatException e) {
            getDataProvider().showMessage("Unknown chat");
        }
    }

    @Override
    public String name() {
        return ":drop";
    }

    @Override
    public String description() {
        return "drop user from chatting";
    }
}
