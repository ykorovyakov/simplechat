package ru.korovyakov.edu.commands;

import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.korovyakov.edu.api.transport.SimpleChatSender;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;

import java.util.EnumSet;

public class SendToChatCommand extends InteractiveChatCommand {
    @Autowired
    @Qualifier("socket_sender")
    private SimpleChatSender sender;

    public SendToChatCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User user) {
        getDataProvider().showMessage("Enter name of the chat to message: ");
        final var chatRoomName = getDataProvider().getNextToken();

        System.out.print("Enter message text: ");
        final String message = getDataProvider().getNextToken();

        sender.sendChatMessage(chatRoomName, message);
    }

    @Override
    public String name() {
        return ":chat";
    }

    @Override
    public String description() {
        return "message to chat room";
    }
}
