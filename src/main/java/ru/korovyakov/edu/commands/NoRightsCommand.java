package ru.korovyakov.edu.commands;

import org.apache.commons.lang3.StringUtils;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;

import java.util.EnumSet;

public class NoRightsCommand extends InteractiveChatCommand {
    public NoRightsCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User user) {
        getDataProvider().showMessage("Has no rights!");
    }

    @Override
    public String name() {
        return StringUtils.EMPTY;
    }

    @Override
    public String description() {
        return StringUtils.EMPTY;
    }
}
