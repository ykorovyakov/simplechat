package ru.korovyakov.edu.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.korovyakov.edu.api.transport.SimpleChatSender;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;

import java.io.IOException;
import java.util.EnumSet;

public class SendPrivateCommand extends InteractiveChatCommand {
    @Autowired
    @Qualifier("socket_sender")
    private SimpleChatSender sender;

    public SendPrivateCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User user) {
        System.out.print("Enter user to send private message: ");
        final String userName = getDataProvider().getNextToken();

        System.out.print("Enter private message text: ");
        final String message = getDataProvider().getNextToken();

        try {
            sender.sendPrivateMessage(userName, message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String name() {
        return ":pr";
    }

    @Override
    public String description() {
        return "send private message to the given user";
    }
}
