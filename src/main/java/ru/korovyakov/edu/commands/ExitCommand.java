package ru.korovyakov.edu.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;
import ru.korovyakov.edu.message.ExitCommandEvent;

import java.util.EnumSet;

public class ExitCommand extends AbstractCommand {
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public ExitCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User user) {
        applicationEventPublisher.publishEvent(new ExitCommandEvent(this));
    }

    @Override
    public String name() {
        return ":exit";
    }

    @Override
    public String description() {
        return "stop chatting";
    }
}
