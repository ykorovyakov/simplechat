package ru.korovyakov.edu.commands;

import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;

import java.util.EnumSet;

public class HelpCommand extends InteractiveChatCommand {
    @Autowired
    @Lazy
    private PermissionHelper permissions;

    public HelpCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User user) {
        final var availableCommands =  permissions.commandsByRole(user.getRole());

        getDataProvider().showMessage("The following commands are available to you:");
        getDataProvider().showMessage("============================================");
        availableCommands.forEach(System.out::println);
    }

    @Override
    public String name() {
        return "?";
    }

    @Override
    public String description() {
        return "get description about available commands";
    }
}
