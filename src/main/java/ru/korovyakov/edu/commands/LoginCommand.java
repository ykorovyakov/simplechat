package ru.korovyakov.edu.commands;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;
import ru.korovyakov.edu.exceptions.UnknownUserException;

import java.util.EnumSet;
import java.util.Objects;

@Component
public class LoginCommand extends InteractiveChatCommand {
    private User user;

    public LoginCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User unused) {
        final String userName = getNameToken();

        user = getService().findUser(userName);
        if(Objects.isNull(user)) {
            user = makeUser(userName);
            getService().createUser(user);
        }
    }

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String description() {
        return StringUtils.EMPTY;
    }

    public User getLoggedUser() {
        return user;
    }

    private String getNameToken() {
        String userName = StringUtils.EMPTY;

        while (userName.isEmpty()) {
            System.out.print("Enter name: ");
            userName = getDataProvider().getNextToken();
            if (userName.isEmpty()) {
                System.out.println("Login name can't be empty, try again!");
            }
        }

        return userName;
    }

    private User makeUser(final String command) {
        final UserRole role = UserRole.fromCommand(command);
        if(role == UserRole.AdminRole) {
            return User.admin();
        }

        return new User(command, role);
    }
}
