package ru.korovyakov.edu.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;

import java.util.EnumSet;

@Getter
@AllArgsConstructor
public abstract class AbstractCommand {
    private EnumSet<UserRole> permissions;

    public abstract void execute(User user);
    public abstract String name();
    public abstract String description();

    public boolean checkPermission(final UserRole role) {
        return permissions.contains(role);
    }

    @Override
    public String toString() {
        return String.format("%s - %s", name(), description());
    }
}
