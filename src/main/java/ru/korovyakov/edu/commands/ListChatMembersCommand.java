package ru.korovyakov.edu.commands;

import lombok.var;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;
import ru.korovyakov.edu.exceptions.UnknownChatException;

import java.util.EnumSet;

public class ListChatMembersCommand extends InteractiveChatCommand {
    public ListChatMembersCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User user) {
//        getService().refresh(user);

        getDataProvider().showMessage("Enter chat to show members:");
        final String chatName = getDataProvider().getNextToken();

        try {
            final var chat = getService().findChat(chatName);
            final var txt = String.format("Chat \"%s\" has next members:", chatName);
            getDataProvider().showMessage(txt);
            getDataProvider().showMessage("=================================");

            chat.getSubscriberList().forEach(System.out::println);
        }
        catch (UnknownChatException ex) {
            getDataProvider().showMessage("Unknown chat");
        }
    }

    @Override
    public String name() {
        return ":members";
    }

    @Override
    public String description() {
        return "lists members of given chat";
    }
}
