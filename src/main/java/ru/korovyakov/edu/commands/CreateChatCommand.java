package ru.korovyakov.edu.commands;

import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;

import java.util.EnumSet;

public class CreateChatCommand extends InteractiveChatCommand {
    public CreateChatCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User user) {
        getDataProvider().showMessage("Enter new chat name:");
        final String chatName = getDataProvider().getNextToken();

        getService().createChat(chatName, user);
    }

    @Override
    public String name() {
        return ":new";
    }

    @Override
    public String description() {
        return "create new chat";
    }
}
