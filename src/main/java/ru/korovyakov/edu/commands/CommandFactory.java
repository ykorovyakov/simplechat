package ru.korovyakov.edu.commands;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.korovyakov.edu.api.DataProvider;
import ru.korovyakov.edu.entity.User;

import java.util.Map;
import java.util.Objects;

@Component
public class CommandFactory {
    @Autowired
    private DataProvider dataProvider;

    @Autowired
    @Qualifier("commandList")
    private Map<String, AbstractCommand> commands;
    @Autowired
    @Qualifier("loginCommand")
    private LoginCommand loginCommand;
    @Autowired
    @Qualifier("defaultCommand")
    private BroadcastMessageCommand defaultCommand;
    @Autowired
    @Qualifier("noRightsCommand")
    private AbstractCommand noRightsCommand;

    public LoginCommand getLoginCommand() {
        return loginCommand;
    }

    public AbstractCommand getNextCommand(final User user) {
        final String token = getToken();
        final AbstractCommand command =  getCommand(token);
        if(!command.checkPermission(user.getRole())) {
            return noRightsCommand;
        }

        return command;
    }

    private AbstractCommand getCommand(final String token) {
        final AbstractCommand command = commands.get(token);
        if(Objects.nonNull(command)) {
            return command;
        }

        defaultCommand.setMessage(token);
        return defaultCommand;
    }

    private String getToken() {
        String token = StringUtils.EMPTY;

        while (token.isEmpty()) {
            token = dataProvider.getNextToken();
        }

        return token;
    }
}
