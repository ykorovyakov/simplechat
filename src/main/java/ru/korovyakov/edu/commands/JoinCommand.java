package ru.korovyakov.edu.commands;

import lombok.var;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;
import ru.korovyakov.edu.exceptions.AlreadyJoinedException;
import ru.korovyakov.edu.exceptions.UnknownChatException;

import java.util.EnumSet;

public class JoinCommand extends InteractiveChatCommand {
    public JoinCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User user) {
        final var dataProvider = getDataProvider();
        dataProvider.showMessage("Enter chat you want to join to");

        final var chatName = dataProvider.getNextToken();

        try {
            getService().joinChat(chatName, user);
        }
        catch (UnknownChatException ex) {
            dataProvider.showMessage("No such chat");
        }
        catch (AlreadyJoinedException ex) {
            dataProvider.showMessage("Already joined");
        }
    }

    @Override
    public String name() {
        return ":join";
    }

    @Override
    public String description() {
        return "join chat";
    }
}
