package ru.korovyakov.edu.commands;

import lombok.var;
import ru.korovyakov.edu.chat.UserRole;
import ru.korovyakov.edu.entity.User;

import java.util.EnumSet;

public class ListChatsCommand extends InteractiveChatCommand {
    public ListChatsCommand(EnumSet<UserRole> permissions) {
        super(permissions);
    }

    @Override
    public void execute(final User user) {
//        getService().refresh(user);

        final var text = String.format("%s subscribed to next chats: ", user.getName());
        getDataProvider().showMessage(text);

        user.getChatNameList().forEach(getDataProvider()::showMessage);
    }

    @Override
    public String name() {
        return ":list";
    }

    @Override
    public String description() {
        return "list chats you subscribed for";
    }
}
