package ru.korovyakov.edu.commands;

import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import ru.korovyakov.edu.api.DataProvider;
import ru.korovyakov.edu.api.service.SimpleChatService;
import ru.korovyakov.edu.chat.UserRole;

import java.util.EnumSet;

public abstract class InteractiveChatCommand extends AbstractCommand {
    @Getter(AccessLevel.PROTECTED)
    @Autowired
    private DataProvider dataProvider;

    @Getter(AccessLevel.PROTECTED)
    @Autowired
    private SimpleChatService service;

    public InteractiveChatCommand(final EnumSet<UserRole> permissions) {
        super(permissions);
    }
}
