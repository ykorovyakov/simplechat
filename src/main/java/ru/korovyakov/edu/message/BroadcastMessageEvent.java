package ru.korovyakov.edu.message;

import org.springframework.context.ApplicationEvent;
import ru.korovyakov.edu.entity.ChatMessage;

public class BroadcastMessageEvent extends ApplicationEvent {
    private ChatMessage message;

    public BroadcastMessageEvent(final Object source, final ChatMessage message) {
        super(source);
        this.message = message;
    }

    public ChatMessage getMessage() {
        return message;
    }
}