package ru.korovyakov.edu.message;

import org.springframework.context.ApplicationEvent;

public class ExitCommandEvent extends ApplicationEvent {
    public ExitCommandEvent(Object source) {
        super(source);
    }
}
