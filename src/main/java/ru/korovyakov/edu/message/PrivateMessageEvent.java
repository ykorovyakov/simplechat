package ru.korovyakov.edu.message;

import org.springframework.context.ApplicationEvent;
import ru.korovyakov.edu.entity.ChatMessage;

public class PrivateMessageEvent extends ApplicationEvent {
  private ChatMessage message;

  public PrivateMessageEvent(final Object source, final ChatMessage message) {
    super(source);
    this.message = message;
  }

  public ChatMessage getMessage() {
    return message;
  }
}
