package ru.korovyakov.edu.message;

import org.springframework.context.ApplicationEvent;
import ru.korovyakov.edu.entity.ChatMessage;

public class ChatMessageEvent extends ApplicationEvent {
    private ChatMessage message;

    public ChatMessageEvent(final Object source, final ChatMessage message) {
        super(source);
        this.message = message;
    }

    public ChatMessage getMessage() {
        return message;
    }
}
