package ru.korovyakov.edu.impl;

import org.springframework.stereotype.Component;
import ru.korovyakov.edu.api.DataProvider;

import java.util.Scanner;

@Component
public class ConsoleDataProvider implements DataProvider {
    private final Scanner scanner = new Scanner(System.in);

    @Override
    public String getNextToken() {
        return scanner.nextLine();
    }

    @Override
    public void showMessage(final String message) {
        System.out.println(message);
    }
}
