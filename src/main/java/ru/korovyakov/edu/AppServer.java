package ru.korovyakov.edu;

import ru.korovyakov.edu.transport.server.SimpleChatServer;

import java.io.IOException;

public class AppServer {
    public static void main(String[] args) throws IOException {
        SimpleChatServer server = new SimpleChatServer();
        server.start();
    }
}
