package ru.korovyakov.edu.transport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.korovyakov.edu.api.transport.SimpleChatSender;
import ru.korovyakov.edu.common.MessageEncoderDecoder;
import ru.korovyakov.edu.entity.User;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

@Component("socket_sender")
public class SocketSimpleChatSender implements SimpleChatSender {
    @Autowired
    private Socket socket;

    private BufferedWriter bufferedWriter;
    private User user;

    @Override
    public void start(User user) throws IOException {
        this.user = user;

        bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    @Override
    public void sendPublicMessage(final String topic, final String text) throws IOException {
        final String data = MessageEncoderDecoder.toPublicString(text, user.getName());

        bufferedWriter.write(data);
        bufferedWriter.newLine();
        bufferedWriter.flush();
    }

    @Override
    public void sendPrivateMessage(String userName, String text) throws IOException {
        final String data = MessageEncoderDecoder.toPrivateString(text, user.getName());

        bufferedWriter.write(data);
        bufferedWriter.newLine();
        bufferedWriter.flush();
    }

    @Override
    public void sendChatMessage(String chat, String text) {
    }
}
