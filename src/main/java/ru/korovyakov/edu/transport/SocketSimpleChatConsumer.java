package ru.korovyakov.edu.transport;

import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.korovyakov.edu.api.transport.SimpleChatConsumer;
import ru.korovyakov.edu.common.MessageEncoderDecoder;
import ru.korovyakov.edu.entity.Chat;
import ru.korovyakov.edu.entity.ChatMessage;
import ru.korovyakov.edu.entity.User;
import ru.korovyakov.edu.runnable.ChatConsumerRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.List;

@Component("socket_consumer")
public class SocketSimpleChatConsumer implements SimpleChatConsumer {
    @Autowired
    private Socket socket;
    private BufferedReader bufferedReader;
    private User user;

    @Autowired
    private ChatConsumerRunner runner;

    @Override
    public void start(User user, List<String> topicList) throws IOException {
        this.user = user;

        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        startAsThread();
    }

    @Override
    public void stop() {
        try {
            runner.shutdown();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void wakeup() {

    }

    @Override
    public void addSubscription(String topic) {

    }

    @Override
    public void removeSubscription(Chat topic, User user) {
    }

    @Override
    public ChatMessage getMessage() throws IOException {
        final String value = bufferedReader.readLine();
        return MessageEncoderDecoder.toMessage(value);
    }

    private void startAsThread() {
        var t = new Thread(runner);
        t.start();
    }
}
