package ru.korovyakov.edu.transport;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;
import ru.korovyakov.edu.api.transport.SimpleChatSender;
import ru.korovyakov.edu.common.CommonRoutines;
import ru.korovyakov.edu.entity.User;

import java.util.Properties;
import java.util.UUID;

@Component("kafka_sender")
public class KafkaChatSender implements SimpleChatSender {
    private KafkaProducer<String, String> rawProducer;
    private User user;

    @Override
    public void start(final User user) {
        rawProducer = producer();
        rawProducer.initTransactions();

        this.user = user;
    }

    @Override
    public void sendPublicMessage(final String topic, final String text) {
        rawProducer.beginTransaction();
        rawProducer.send(new ProducerRecord<>(topic, user.getName(), text));
        rawProducer.commitTransaction();

    }

    @Override
    public void sendPrivateMessage(final String userName, final String text) {
        rawProducer.beginTransaction();
        final String topicName = CommonRoutines.makePrivateTopicName(userName);
        rawProducer.send(new ProducerRecord<>(topicName, user.getName(), text));
        rawProducer.commitTransaction();
    }

    @Override
    public void sendChatMessage(final String chat, final String text) {
        rawProducer.beginTransaction();
        rawProducer.send(new ProducerRecord<>(chat, user.getName(), text));
        rawProducer.commitTransaction();
    }

    private static KafkaProducer<String, String> producer() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("transactional.id", UUID.randomUUID().toString());
        return new KafkaProducer<>(props, new StringSerializer(), new StringSerializer());
    }
}
