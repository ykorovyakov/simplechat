package ru.korovyakov.edu.transport.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleChatServer {
    private List<ClientHandler> clientHandlers = new ArrayList<>();
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    public void start() throws IOException {
        try (ServerSocket server = new ServerSocket(4888)) {
            System.out.println("Server started...");

            while (true) {
                final Socket client = server.accept();
                System.out.println("Client connected!");

                final ClientHandler clientHandler = new ClientHandler(client, clientHandlers);
                executorService.submit(clientHandler);
//                executorService.shutdown();

                clientHandlers.add(clientHandler);
            }
        }
    }
}
