package ru.korovyakov.edu.transport.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

class ClientHandler implements Runnable {
    private boolean isActive = true;

    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    private List<ClientHandler> other;

    private Socket socket;

    public ClientHandler(final Socket socket, final List<ClientHandler> other) throws IOException {
        this.socket = socket;
        this.other = other;

        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    @Override
    public void run() {
        while(isActive)
        {
            try
            {
                final String received = bufferedReader.readLine();
                if(Objects.isNull(received)) {
                    stop();
                    continue;
                }

                for(ClientHandler handler : other) {
                    if(handler != this) {
                        handler.bufferedWriter.write(received);
                        handler.bufferedWriter.newLine();
                        handler.bufferedWriter.flush();
                    }
                }

                if(received.equals("exit")){
                    stop();
                    break;
                }

            } catch (IOException e) {
                try {
                    stop();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void stop() throws IOException {
        isActive = false;
        other.remove(this);
        socket.close();
    }
}