package ru.korovyakov.edu.transport;

import lombok.var;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.korovyakov.edu.api.transport.SimpleChatConsumer;
import ru.korovyakov.edu.common.CommonRoutines;
import ru.korovyakov.edu.entity.Chat;
import ru.korovyakov.edu.entity.ChatMessage;
import ru.korovyakov.edu.entity.User;
import ru.korovyakov.edu.runnable.ChatConsumerRunner;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import static ru.korovyakov.edu.bootstrap.SimpleChatApp.PUBLIC_TOPIC_NAME;

@Component("kafka")
public class KafkaChatConsumer implements SimpleChatConsumer {
    private static final Duration POLL_TIMEOUT = Duration.ofMillis(10000);

    @Autowired
    private ChatConsumerRunner runner;

    private KafkaConsumer<String, String> rawConsumer;
    private User user;

    @Override
    public void start(final User user, List<String> topicListRaw) {
        this.user = user;

        rawConsumer = consumer();

        final var topics = new ArrayList<String>(topicListRaw);
        topics.add(CommonRoutines.makePrivateTopicName(user.getName()));
        topics.add(PUBLIC_TOPIC_NAME);

        rawConsumer.subscribe(topics);

        startAsThread();
    }

    @Override
    public void stop() {
        runner.shutdown();
    }

    @Override
    public void addSubscription(final String topic) {
        final var chatNameList = user.getChatNameList();

        chatNameList.add(topic);
        chatNameList.add(PUBLIC_TOPIC_NAME);

        runner.shutdown();

        start(user, chatNameList);
    }

    @Override
    public void removeSubscription(final Chat chat, final User user) {
        final var chatNameList = user.getChatNameList();
        chatNameList.remove(chat.getName());

        runner.shutdown();

        start(user, chatNameList);
    }

    @Override
    public ChatMessage getMessage() {
        final var records =  rawConsumer.poll(POLL_TIMEOUT);
        if(records.isEmpty()) {
            return null;
        }

        for (var rec : records) {
            final String from = rec.key();
            if (user.getName().equals(from)) {
                continue;
            }
        }

        return null;
    }

    @Override
    public void wakeup() {
        rawConsumer.wakeup();
    }

    private void startAsThread() {
        var t = new Thread(runner);
        t.start();
    }

    private static KafkaConsumer<String, String> consumer() {
        Properties props = new Properties();
        props.setProperty("bootstrap.servers", "localhost:9092");
        props.setProperty("group.id", UUID.randomUUID().toString());
        props.setProperty("enable.auto.commit", "true");
        props.setProperty("auto.commit.interval.ms", "1000");
        props.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        return new KafkaConsumer<>(props);
    }
}
