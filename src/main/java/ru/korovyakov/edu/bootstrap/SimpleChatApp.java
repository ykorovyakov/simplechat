package ru.korovyakov.edu.bootstrap;

import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.korovyakov.edu.api.transport.SimpleChatConsumer;
import ru.korovyakov.edu.api.transport.SimpleChatSender;
import ru.korovyakov.edu.commands.CommandFactory;
import ru.korovyakov.edu.entity.User;
import ru.korovyakov.edu.message.ExitCommandEvent;

import java.io.IOException;

@Component
public class SimpleChatApp {
    public static final String PUBLIC_TOPIC_NAME = "my_topic_test";

    private static final String PRIVATE_MESSAGE_FORMAT = "[private from %s] %s";
    private static final String PUBLIC_MESSAGE_FORMAT = "[%s] %s";
    private static final String CHAT_MESSAGE_FORMAT = "[%s][%s] %s";

    @Autowired
    @Qualifier("socket_consumer")
    private SimpleChatConsumer consumer;
    @Autowired
    @Qualifier("socket_sender")
    private SimpleChatSender sender;
    @Autowired
    private CommandFactory commandFactory;

    private User currentUser;
    private boolean stop = false;

    public void startChat() throws IOException {
        login();
        loop();
        logout();
    }

    @EventListener
    private void onExitCommand(final ExitCommandEvent msg) {
        stop = true;
        consumer.stop();
    }

    private void loop() {
        while (!stop) {
            final var command = commandFactory.getNextCommand(currentUser);
            command.execute(currentUser);
        }
    }

    private void login() throws IOException {
        final var lg = commandFactory.getLoginCommand();
        lg.execute(User.empty());

        currentUser = lg.getLoggedUser();

        sender.start(currentUser);
        consumer.start(currentUser, currentUser.getChatNameList());
    }

    private void logout() {
        consumer.stop();
    }
}
