package ru.korovyakov.edu;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.korovyakov.edu.bootstrap.SimpleChatApp;
import ru.korovyakov.edu.config.SimpleChatConfig;

public class App {
    public static void main(String[] args) throws Exception {
        final ApplicationContext context = new AnnotationConfigApplicationContext(SimpleChatConfig.class);

        final SimpleChatApp app = context.getBean(SimpleChatApp.class);
        app.startChat();
    }
}
